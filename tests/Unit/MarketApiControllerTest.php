<?php

use  \App\Entities\Product;

class MarketApiControllerTest extends \Tests\TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->app->bind(\App\Repositories\Interfaces\ProductRepositoryInterface::class, function () {
            return new \App\Repositories\TestProductRepository(self::getProducts());
        });
    }

    public function testShowList()
    {
        $data = $this->json('get', 'api/items')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'price',
                    'user_id',
                ]
            ])
            ->json();

        $this->assertNotEmpty($data);

        foreach ($data as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('name', $item);
            $this->assertArrayHasKey('price', $item);
            $this->assertArrayHasKey('user_id', $item);
        }
    }

    public function testShowProduct()
    {
        $this->json('get', 'api/items/58')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'price',
                    'user_id',
                ]
            ]);

        $this->json('get', 'api/items/5')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(500);
    }

    public function testDeleteProduct()
    {
        $this->json('delete', 'api/items/1')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure(['message']);

        $this->json('delete', 'api/items/5')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(500);
    }

    public function testStoreProduct()
    {
        $data = [
            'name' => 'test product',
            'price' => 500,
            'user_id' => 1,
        ];
        $response = $this->json('post', 'api/items', $data)
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'price',
                    'user_id',
                ]
            ])->json();

        $this->assertNotEmpty($response);

        $this->assertArrayHasKey('id', $response['data']);
        $this->assertArrayHasKey('name', $response['data']);
        $this->assertArrayHasKey('price', $response['data']);
        $this->assertArrayHasKey('user_id', $response['data']);

        $this->assertEquals('test product', $response['data']['name']);
        $this->assertEquals(500, $response['data']['price']);
        $this->assertEquals(1, $response['data']['user_id']);

        Product::destroy($response['data']['id']);

        $failedData = [
            'price' => 500,
            'user_id' => 1,
        ];
        $this->json('post', 'api/items', $failedData)
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(500);

    }

    private static function getProducts(): array
    {
        return [
            new Product([
                'id' => 2,
                'name' => 'product2',
                'price' => 200.22,
                'user_id' => 1
            ]),
            new Product([
                'id' => 1,
                'name' => 'product1',
                'price' => 100.11,
                'user_id' => 1
            ]),
            new Product([
                'id' => 3,
                'name' => 'product3',
                'price' => 300.33,
                'user_id' => 3
            ]),
            new Product([
                'id' => 58,
                'name' => 'product4',
                'price' => 400.44,
                'user_id' => 3
            ])
        ];
    }
}