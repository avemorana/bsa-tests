<?php

class MarketServiceTest extends \Tests\TestCase
{
    public function testGetProductById()
    {
        $productRepositoryStub = $this->createMock(\App\Repositories\ProductRepository::class);
        $productRepositoryStub->method('findById')->will($this->returnCallback(
            function ($id) {
                if ($id == 5) {
                    return \App\Entities\Product::make(['id' => 5]);
                }
                return null;
            }
        ));
        $marketService = new \App\Services\MarketService($productRepositoryStub);
        $this->assertEquals(5, ($marketService->getProductById(5))->id);
        $this->expectException(Exception::class);
        $this->assertEquals(5, ($marketService->getProductById(12))->id);
    }

    public function testGetProductList()
    {
        $productRepositoryStub = $this->createMock(\App\Repositories\ProductRepository::class);
        $productRepositoryStub->method('findAll')->willReturn(\Illuminate\Database\Eloquent\Collection::make([]));
        $marketService = new \App\Services\MarketService($productRepositoryStub);
        $this->assertEquals(\Illuminate\Database\Eloquent\Collection::make([]), $marketService->getProductList());

        $productCollection = \Illuminate\Database\Eloquent\Collection::make([
            \App\Entities\Product::make(['id' => 5]),
            \App\Entities\Product::make(['id' => 6]),
            \App\Entities\Product::make(['id' => 7]),
        ]);
        $productRepositoryStub2 = $this->createMock(\App\Repositories\ProductRepository::class);
        $productRepositoryStub2->method('findAll')->willReturn($productCollection);
        $marketService = new \App\Services\MarketService($productRepositoryStub2);
        $this->assertEquals($productCollection, $marketService->getProductList());
    }

    public function testGetProductsByUserId()
    {
        $productRepositoryStub = $this->createMock(\App\Repositories\ProductRepository::class);
        $productRepositoryStub->method('findByUserId')->willReturn(\Illuminate\Database\Eloquent\Collection::make([]));
        $marketService = new \App\Services\MarketService($productRepositoryStub);
        $this->assertEquals(\Illuminate\Database\Eloquent\Collection::make([]), $marketService->getProductsByUserId(2));

        $productCollection = \Illuminate\Database\Eloquent\Collection::make([
            \App\Entities\Product::make(['id' => 5]),
            \App\Entities\Product::make(['id' => 6]),
            \App\Entities\Product::make(['id' => 7]),
        ]);
        $productRepositoryStub2 = $this->createMock(\App\Repositories\ProductRepository::class);
        $productRepositoryStub2->method('findByUserId')->willReturn($productCollection);
        $marketService = new \App\Services\MarketService($productRepositoryStub2);
        $this->assertEquals($productCollection, $marketService->getProductsByUserId(2));
    }

    public function testStoreProduct()
    {
        $product = new \App\Entities\Product([
            'user_id' => \Illuminate\Support\Facades\Auth::id(),
            'name' => 'TestProduct',
            'price' => 12.75
        ]);
        $productRepositoryStub = $this->createMock(\App\Repositories\ProductRepository::class);
        $productRepositoryStub->method('store')->will($this->returnArgument(0));

        $request = new \Illuminate\Http\Request(['product_name' => 'TestProduct', 'product_price' => 12.75]);
        $marketService = new \App\Services\MarketService($productRepositoryStub);
        $this->assertEquals($product, $marketService->storeProduct($request));
    }

}
