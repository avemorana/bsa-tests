<?php

namespace App\Repositories;

use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class TestProductRepository implements ProductRepositoryInterface
{
    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }


    public function findAll(): Collection
    {
        return Collection::make($this->products);
    }

    public function findById(int $id): ?Product
    {
        foreach ($this->products as $item) {
            if ($item->id == $id) {
                return $item;
            }
        }
        return null;
    }

    public function findByUserId(int $userId): Collection
    {
        $result = [];
        foreach ($this->products as $item) {
            if ($item->user_id == $userId) {
                $result[] = $item;
            }
        }
        return Collection::make($result);
    }

    public function store(Product $product): Product
    {
        $this->products[] = $product;
        return $product;
    }

    public function delete(Product $product): void
    {
        foreach ($this->products as $item) {
            if ($product == $item) {
                unset($item);
                break;
            }
        }
    }
}