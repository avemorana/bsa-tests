<?php

namespace App\Http\Controllers;

use App\Entities\Product;
use App\Http\Resources\ProductPresenter;
use App\Repositories\ProductRepository;
use App\Repositories\TestProductRepository;
use App\User;
use Illuminate\Http\Request;
use App\Services\MarketService;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList()
    {
        $products = $this->marketService->getProductList();
        return $products->map(function ($product) {
            return new ProductPresenter($product);
        });
    }

    public function store(Request $request)
    {
        $userId = $request->input('user_id');
        $user = User::find($userId);
        if (!$user){
            throw new \Exception("No user with id: $userId");
        }
        $product = new Product([
            'user_id' => $userId,
            'name' => $request->input('name'),
            'price' => $request->input('price')
        ]);

        $productRepository = new ProductRepository();
        return new ProductPresenter($productRepository->store($product));
    }

    public function showProduct(int $id)
    {
        $product = $this->marketService->getProductById($id);
        return new ProductPresenter($product);
    }

    public function delete(Request $request)
    {
        $this->marketService->deleteProduct($request);
        return ['message' => 'Product was deleted'];
    }
}
